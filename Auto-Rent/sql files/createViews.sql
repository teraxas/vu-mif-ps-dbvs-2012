CREATE VIEW kajo0241.KasKamIsnuomota AS
SELECT A.ValstybNr, A.Pavadinimas, K.Vardas, K.Pavarde, N.SutartiesNr, N.Nuo, N.Iki
FROM kajo0241.Automobilis AS A
	JOIN kajo0241.Nuoma AS N ON A.ValstybNr = N.ValstybNr
	JOIN kajo0241.Klientas AS K ON K.AK = N.KlientoAK
WHERE N.Iki > CURRENT_DATE;
	
CREATE VIEW kajo0241.VisuAutoInfo AS
SELECT A.ValstybNr, M.Gamintojas, A.Pavadinimas, A.Metai, M.VariklioTipas, M.GaliaKW, A.Verte, A.DienosKaina
FROM kajo0241.Modelis AS M
	JOIN kajo0241.Automobilis AS A ON A.Pavadinimas = M.Pavadinimas;
