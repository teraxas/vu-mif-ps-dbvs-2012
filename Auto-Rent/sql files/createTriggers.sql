--Ar galima išnuomoti mašiną? Galima, jei ji jau grąžinta.
CREATE FUNCTION kajo0241.ArGalimaPaimti()
RETURNS "trigger" AS
'BEGIN
IF (SELECT COUNT (*)
		FROM kajo0241.Nuoma AS N
		WHERE ValstybNr = New.ValstybNr
			AND Iki >= New.Nuo) >=1
	THEN RAISE EXCEPTION ''Automobilis jau isnuomotas'';
	END IF;
RETURN NEW;
END;'
LANGUAGE 'plpgsql';

CREATE TRIGGER ArGalimaPaimti
BEFORE INSERT OR UPDATE ON kajo0241.Nuoma
FOR EACH ROW
EXECUTE PROCEDURE kajo0241.ArGalimaPaimti();

CREATE FUNCTION kajo0241.GamybosMetai()
RETURNS "trigger" AS
'BEGIN
IF (New.Metai < 1900 OR New.Metai > EXTRACT(year FROM current_date)) 
	THEN RAISE EXCEPTION ''Pagaminimo metai negali buti senesni nei 1900 ir naujesni nei einamieji metai'';
	END IF;
RETURN NEW;
END;'
LANGUAGE 'plpgsql';

CREATE TRIGGER GamybosMetai
BEFORE INSERT OR UPDATE ON kajo0241.Automobilis
FOR EACH ROW
EXECUTE PROCEDURE kajo0241.GamybosMetai();
