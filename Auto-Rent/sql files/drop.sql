--trigeriai
DROP TRIGGER ArGalimaPaimti ON kajo0241.Nuoma;
DROP FUNCTION ArGalimaPaimti();
DROP TRIGGER GamybosMetai ON kajo0241.Automobilis;
DROP FUNCTION GamybosMetai();

--indeksai
DROP INDEX kajo0241.IndexKlientui;
DROP INDEX kajo0241.IndexAK;

--Virt. lentelės
DROP VIEW kajo0241.KasKamIsnuomota;
DROP VIEW kajo0241.VisuAutoInfo;

--lenteles
DROP TABLE kajo0241.Nuoma;
DROP TABLE kajo0241.Klientas;
DROP TABLE kajo0241.Automobilis;
DROP TABLE kajo0241.Modelis;


