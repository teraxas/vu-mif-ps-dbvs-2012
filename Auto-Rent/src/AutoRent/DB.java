package AutoRent;

import java.sql.*;
import java.text.ParseException;

public class DB {

	public Connection con = null;
	
	private PreparedStatement psInsertModelis;
	private PreparedStatement psDeleteModelis;
	private PreparedStatement psSelectModelis;
	
	private PreparedStatement psInsertAutomobilis;
	private PreparedStatement psUpdateAutomobilis;
	private PreparedStatement psDeleteAutomobilis;
	private PreparedStatement psSelectAutomobilis;
	
	private PreparedStatement psInsertKlientas;
	private PreparedStatement psUpdateKlientas;
	private PreparedStatement psDeleteKlientas;
	private PreparedStatement psSelectKlientas;
	
	private PreparedStatement psInsertNuoma;
	private PreparedStatement psUpdateNuoma;
	private PreparedStatement psDeleteNuoma;
	private PreparedStatement psSelectNuoma;
	
	public DB(String url, String user, String pass) {
        try {
            loadDriver();
            getConnection(url, user, pass);
        } catch (Exception e) {
            System.out.println(e.getMessage());
            System.exit(1);
        }
    }

	private void loadDriver() throws Exception {
        try {
            Class.forName("org.postgresql.Driver");
        }
        catch (ClassNotFoundException cnfe) {
            throw new Exception("Loading driver classs FAILED!!!");
        }
    }
	
	/**
	 * get connection to db
	 * @param url
	 * @param user
	 * @param pass
	 * @return
	 * @throws Exception
	 */
	private Connection getConnection(String url, String user, String pass) throws Exception {
        try {
            con = DriverManager.getConnection(url, user, pass);
        }
        catch (SQLException sqle) {
            con = null;
            throw new Exception("Getting connection FAILED!!!");
        }
        return con;
    }

	public boolean isConnected() {
        if (con != null) {
            return true;
        }  
        return false;
    }
	
	public void closeConnection() throws SQLException {
        con.close();
    }
	
	/**
	 * Initializes prepared statements
	 * @throws SQLException
	 */
	public void initPrepStatements() throws SQLException {
		//Modelis
		this.psInsertModelis = this.con.prepareStatement(
				"INSERT INTO kajo0241.Modelis VALUES(?, ?, ?, ?, ?)");
		this.psDeleteModelis = this.con.prepareStatement(
				"DELETE FROM kajo0241.Modelis WHERE Pavadinimas = ?");
		this.psSelectModelis = this.con.prepareStatement(
				"SELECT * FROM kajo0241.Modelis");
		
		//Automobilis
		this.psInsertAutomobilis = this.con.prepareStatement(
				"INSERT INTO kajo0241.Automobilis VALUES(?, ?, ?, ?, ?, ?)");
		this.psUpdateAutomobilis = this.con.prepareStatement(
				"UPDATE kajo0241.Automobilis SET DienosKaina = ?, " +
				"Verte = ? WHERE ValstybNr = ?");
		this.psDeleteAutomobilis = this.con.prepareStatement(
				"DELETE FROM kajo0241.Automobilis WHERE ValstybNr = ?");
		this.psSelectAutomobilis = this.con.prepareStatement(
				"SELECT ValstybNr, Pavadinimas, Spalva, Metai, DienosKaina, Verte " +
				"FROM kajo0241.Automobilis WHERE ValstybNr = ?");
		
		//Klientas
		this.psInsertKlientas = this.con.prepareStatement(
				"INSERT INTO kajo0241.Klientas VALUES(?, ?, ?, ?)");
		this.psUpdateKlientas = this.con.prepareStatement(
				"UPDATE kajo0241.Klientas SET Telefonas = ? WHERE AK = ?");
		this.psDeleteKlientas = this.con.prepareStatement(
				"DELETE FROM kajo0241.Modelis WHERE AK = ?");
		this.psSelectKlientas = this.con.prepareStatement(
				"SELECT AK, Vardas, Pavarde, Telefonas" +
				"FROM kajo0241.Klientas WHERE AK = ?");
		
		//Nuoma
		this.psInsertNuoma = this.con.prepareStatement(
				"INSERT INTO kajo0241.Nuoma VALUES(DEFAULT, ?, ?, ?, ?, ?)");
		this.psUpdateNuoma = this.con.prepareStatement(
				"UPDATE kajo0241.Nuoma SET Iki = ?, Suma = ?, WHERE SutartiesNr = ?");
		this.psDeleteNuoma = this.con.prepareStatement(
				"DELETE FROM kajo0241.Nuoma WHERE SutartiesNr = ?");
		this.psSelectNuoma = this.con.prepareStatement(
				"SELECT SutartiesNr, ValstybNr, Nuo, Iki, Suma, KlientoAK" +
				"FROM kajo0241.Nuoma");
	}
	
	/**
	 * closes prepared statements
	 */
	public void closePreparedStatements() {
        try {      
            this.psInsertAutomobilis.close();
            this.psInsertKlientas.close();
            this.psInsertModelis.close();
            this.psInsertNuoma.close();
            
            this.psUpdateAutomobilis.close();
            this.psUpdateKlientas.close();
            this.psUpdateNuoma.close();
            
            this.psDeleteAutomobilis.close();
            this.psDeleteKlientas.close();
            this.psDeleteModelis.close();
            this.psDeleteNuoma.close();
            
            this.psSelectAutomobilis.close();
            this.psSelectKlientas.close();
            this.psSelectModelis.close();
            this.psSelectNuoma.close();
        } catch (SQLException sqle) {
            System.out.println(sqle);
        }
    }
	
	public void insertModelis(
			String gamintojas, String pavadinimas, String variklioTipas,
			int galia, int greitis) throws SQLException {
		psInsertModelis.setString(1, pavadinimas);
		psInsertModelis.setString(2, gamintojas);
		psInsertModelis.setString(3, variklioTipas);
		psInsertModelis.setInt(4, galia);
		psInsertModelis.setInt(5, greitis);
		
		psInsertModelis.executeUpdate();
	}
	
	public void deleteModelis(String pavadinimas) throws SQLException{
		psDeleteModelis.setString(1, pavadinimas);
		psDeleteModelis.executeUpdate();
	}
	
	public ResultSet selectModelis() throws SQLException{
		return psSelectModelis.executeQuery();
	}
	
	public void insertAutomobilis(
			String valstybNr, String pavadinimas, String spalva, 
			int metai, int dienosKaina, int verte) throws SQLException{
		psInsertAutomobilis.setString(1, valstybNr);
		psInsertAutomobilis.setString(2, pavadinimas);
		psInsertAutomobilis.setString(3, spalva);
		psInsertAutomobilis.setInt(4, metai);
		psInsertAutomobilis.setInt(5, dienosKaina);
		psInsertAutomobilis.setInt(6, verte);
		
		psInsertAutomobilis.executeUpdate();
	}
	
	public void updateAutomobilis(
			int dienosKaina, int verte, String valstybNr) throws SQLException{
		psUpdateAutomobilis.setInt(1, dienosKaina);
		psUpdateAutomobilis.setInt(2, verte);
		psUpdateAutomobilis.setString(3, valstybNr);
		
		psUpdateAutomobilis.executeUpdate();
	}
	
	public void deleteAutomobilis(String valstybNr) throws SQLException{
		psDeleteAutomobilis.setString(1, valstybNr);
		
		psDeleteAutomobilis.executeUpdate();
	}
	
	public ResultSet selectAutomobilis(String valstybNr) throws SQLException{
		psSelectAutomobilis.setString(1, valstybNr);
		return psSelectAutomobilis.executeQuery();
	}
	
	public ResultSet selectAutomobilis() throws SQLException {
		PreparedStatement stmt = this.con.prepareStatement(
				"SELECT * FROM kajo0241.Automobilis");
		return stmt.executeQuery();
	}
	
	public void insertKlientas(String ak, String vardas, 
			String pavarde, String telefonas) throws SQLException{
		psInsertKlientas.setString(1, ak);
		psInsertKlientas.setString(2, vardas);
		psInsertKlientas.setString(3, pavarde);
		psInsertKlientas.setString(4, telefonas);
		
		psInsertKlientas.executeUpdate();
	}
	
	public void insertKlientas(String ak, String vardas, 
			String pavarde, String telefonas, String valstybNr, String nuo, 
			String iki, int suma) throws SQLException{
		con.setAutoCommit(false);//Hmmm...
		
		insertKlientas(ak, vardas, pavarde, telefonas);
		
		
		
		System.out.println("Įdėjom klientą.");

		Statement stmt = this.con.createStatement();
        ResultSet r = stmt.executeQuery("SELECT * FROM kajo0241.Automobilis WHERE ValstybNr = '" + valstybNr + "'");
        if (r.next()){
        	try {
        		System.out.println("Dedam nuomą...");
				insertNuoma(valstybNr, nuo, iki, suma, ak);
				con.commit();
			} catch (ParseException e) {
				e.printStackTrace();
			}
        } else {
        	con.rollback();
            throw new SQLException("Tokio automobilio nėra");
        }
        
        con.setAutoCommit(true);
	}
	
	public void updateKlientas(String telNr, String ak) throws SQLException{
		psUpdateKlientas.setString(1, telNr);
		psUpdateKlientas.setString(2, ak);
		
		psUpdateKlientas.executeUpdate();
	}
	
	public void deleteKlientas(String ak) throws SQLException{
		psDeleteKlientas.setString(1, ak);
		
		psDeleteKlientas.executeUpdate();
	}
	
	public ResultSet selectKlientas(String ak) throws SQLException{
		psSelectKlientas.setString(1, ak);
		return psSelectKlientas.executeQuery();
	}
	
	public ResultSet selectKlientas() throws SQLException {
		PreparedStatement stmt = this.con.prepareStatement(
				"SELECT * FROM kajo0241.Klientas");
		return stmt.executeQuery();
	}
	
	public void insertNuoma(String valstybNr, String nuo, 
			String iki, int suma, String ak) throws SQLException, ParseException{
		
		
		
		psInsertNuoma.setString(1, valstybNr);
		psInsertNuoma.setDate(2, Date.valueOf(nuo));
		psInsertNuoma.setDate(3, Date.valueOf(iki));
		psInsertNuoma.setInt(4, suma);
		psInsertNuoma.setString(5, ak);
		
		psInsertNuoma.executeUpdate();
	}
	
	public void updateNuoma(String iki, int suma, int sutartiesNr) throws SQLException{
		psUpdateNuoma.setDate(1, Date.valueOf(iki));
		psUpdateNuoma.setInt(2, suma);
		psUpdateNuoma.setInt(3, sutartiesNr);
		
		psUpdateNuoma.executeUpdate();
	}
	
	public void deleteNuoma(int sutartiesNr) throws SQLException{
		psDeleteNuoma.setInt(1, sutartiesNr);
		
		psDeleteNuoma.executeUpdate();
	}
	
	public ResultSet selectNuoma(int sutartiesNr) throws SQLException{
		psSelectNuoma.setInt(1, sutartiesNr);
		return psSelectNuoma.executeQuery();
	}
	
	public ResultSet selectNuoma() throws SQLException{
		PreparedStatement stmt = this.con.prepareStatement(
				"SELECT * FROM kajo0241.Nuoma");
		return stmt.executeQuery();
	}
	
}
