package AutoRent;

import java.sql.SQLException;

public class Main {
	private static String url = "jdbc:postgresql://pgsql2.mif/studentu";
    private static String username = "xxxx0000";
    private static String password = "xxxx0000";
    
    public static void main(String[] args) {  
        DB db = new DB(url, username, password);
        if (db.isConnected()) {
            System.out.println("Successfully connected to Postgres Database"); 
            
            try {
				db.initPrepStatements();
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
            
            @SuppressWarnings("unused")
			UI ui = new UI(db);
            
            db.closePreparedStatements();
            
            try {
                db.closeConnection(); 
            } catch (SQLException sqle) {
                System.out.println(sqle);
            }
        }
    }

}
