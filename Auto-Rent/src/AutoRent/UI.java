package AutoRent;

import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.text.ParseException;
import java.util.Scanner;

public class UI {

	DB db;
	
	public UI(DB db) {
		this.db = db;
		
		initUI();
	}

	private void initUI() {
		String key = "";
		
		Scanner in = new Scanner(System.in);
		
		do {
			printMenu();
			key = in.nextLine();
			
			if (key != null)
				if (!key.equals("q")){
					doStuff(key);
				} else {
					System.out.println("cya");
				}
		} while (!key.equals("q"));
		
		in.close();
	}
	
	private void doStuff(String key){
		@SuppressWarnings("resource")
		Scanner in = new Scanner(System.in);
		
		@SuppressWarnings("unused")
		ResultSet rs;
		
		int k = Integer.valueOf(key);
		
		String pavadinimas;
		int kaina;
		int verte;
		String valstybNr;
		String ak;
		String nuo;
		String iki;
		int suma;
		
		switch (k){
		case 0:
			System.out.println("ĮVESK NAUJO MODELIO DUOMENIS");
			System.out.println(" Pavadinimas:");
			pavadinimas = in.nextLine();
			System.out.println(" Gamintojas:");
			String gamintojas = in.nextLine();
			System.out.println(" Variklio tipas:");
			String variklis = in.nextLine();
			System.out.println(" Galia:");
			int galia =  Integer.valueOf(in.nextLine());
			System.out.println(" Greitis:");
			int greitis = Integer.valueOf(in.nextLine());
			try {
				db.insertModelis(gamintojas, pavadinimas, variklis, galia, greitis);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 1:
			System.out.println("   MODELIAI:");
			try {
				printResultSet(db.selectModelis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 2:
			System.out.println("ĮVESK NAUJO AUTOMOBILIO DUOMENIS");
			System.out.println(" Valstybinis numeris:");
			valstybNr = in.nextLine();
			try {
				printResultSet(db.selectModelis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println(" Pavadinimas (įvesti modeliai aukščiau):");
			pavadinimas = in.nextLine();
			System.out.println(" Spalva:");
			String spalva = in.nextLine();
			System.out.println(" Metai:");
			int metai = Integer.valueOf(in.nextLine());
			System.out.println(" Dienos kaina:");
			kaina = Integer.valueOf(in.nextLine());
			System.out.println(" Vertė:");
			verte = Integer.valueOf(in.nextLine());
			
			try {
				db.insertAutomobilis(valstybNr, pavadinimas, spalva, metai, kaina, verte);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 3:
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("ĮVESK KEIČIAMO AUTOMOBILIO VALSTYB. NR.");
			System.out.println("   (sąrašas aukščiau)");
			valstybNr = in.nextLine();
			System.out.println("ĮVESK NAUJĄ AUTOMOBILIO DIENOS KAINĄ:");
			kaina = Integer.valueOf(in.nextLine());
			System.out.println("IR VERTĘ:");
			verte = Integer.valueOf(in.nextLine());
			
			try {
				db.updateAutomobilis(kaina, verte, valstybNr);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 4:
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("ĮVESK TRINAMO AUTOMOBILIO VALSTYB. NR.");
			System.out.println("   (sąrašas aukščiau)");
			valstybNr = in.nextLine();
			try {
				db.deleteAutomobilis(valstybNr);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 5:
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("ĮVESK IEŠKOMO AUTOMOBILIO VALSTYB. NR.");
			System.out.println("   (sąrašas aukščiau)");
			valstybNr = in.nextLine();
			try {
				printResultSet(db.selectAutomobilis(valstybNr));
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 6:
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 7:
			System.out.println("ĮVESK NAUJO KLIENTO DUOMENIS");
			System.out.println("   ak:");
			ak = in.nextLine();
			System.out.println("   Vardas:");
			String vardas = in.nextLine();
			System.out.println("   Pavardė:");
			String pavarde = in.nextLine();
			System.out.println("   Telefonas:");
			String telNr = in.nextLine();
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("   Valstyb. nr. (sąrašas aukščiau):");
			valstybNr = in.nextLine();
			System.out.println("   Nuo (yyyy-MM-dd):");
			nuo = in.nextLine();
			System.out.println("   Iki (yyyy-MM-dd):");
			iki = in.nextLine();
			System.out.println("   Suma:");
			suma = Integer.valueOf(in.nextLine());
			
			try {
				db.insertKlientas(ak, vardas, pavarde, telNr, valstybNr, nuo, iki, suma);
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 8:
			try {
				printResultSet(db.selectKlientas());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			break;
		case 9:
			System.out.println("ĮVESK NAUJOS NUOMOS DUOMENIS");
			try {
				printResultSet(db.selectAutomobilis());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			System.out.println("   Valstyb. nr. (sąrašas aukščiau):");
			valstybNr = in.nextLine();
			System.out.println("   Nuo (yyyy-MM-dd):");
			nuo = in.nextLine();
			System.out.println("   Iki (yyyy-MM-dd):");
			iki = in.nextLine();
			System.out.println("   Suma:");
			suma = Integer.valueOf(in.nextLine());
			try {
				printResultSet(db.selectKlientas());
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("   Kliento asmens kodas (klientų sąrašas aukščiau):");
			ak = in.nextLine();
			
			try {
				try {
					db.insertNuoma(valstybNr, nuo, iki, suma, ak);
				} catch (ParseException e) {
					System.out.println(e.getMessage());
				}
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		case 10:
			System.out.println("   NUOMOS:");
			try {
				printResultSet(db.selectNuoma());
			} catch (SQLException e) {
				System.out.println(e.getMessage());
			}
			break;
		}
	}

	private void printMenu() {
		System.out.println("---AutoNuoma---");
		
		System.out.println("\n     Modeliai:");
		System.out.println("0  - pridėti naują automobilio modelį");
		System.out.println("1  - parodyti visus modelius");
		
		System.out.println("\n     Automobiliai:");
		System.out.println("2  - pridėti naują automobilį");
		System.out.println("3  - pakeisti automobilio dienos kainą ir vertę");
		System.out.println("4  - ištrinti automobilį");
		System.out.println("5  - rasti automobilį pagal valstyb. nr.");
		System.out.println("6  - parodyti visus automobilius");
		
		System.out.println("\n     Klientai:");
		System.out.println("7  - pridėti naują klientą");
		System.out.println("8  - parodyti visus klientus");
		
		System.out.println("\n     Nuomos:");
		System.out.println("9  - pridėti naują nuomą");
		System.out.println("10 - parodyti visas sutartis");
		
		System.out.println("q  - baigti darbą");
		
		System.out.println("\nĮvesk komandos kodą:");
	}
	
    public static void printResultSet (ResultSet rs) throws SQLException {
        if (rs != null) {
            ResultSetMetaData md = rs.getMetaData ( );
            int columnsCount = md.getColumnCount ( );
            int rowsCount = 0;
            int[ ] width = new int[columnsCount + 1];
            StringBuilder b = new StringBuilder ( );

            for (int i = 1; i <= columnsCount; i++){
                width[i] = md.getColumnDisplaySize (i);
                if (width[i] < md.getColumnName (i).length ( ))
                    width[i] = md.getColumnName (i).length ( );
                if (width[i] < 4 && md.isNullable (i) != 0)
                    width[i] = 4;
            }

            b.append ("+");
            for (int i = 1; i <= columnsCount; i++){
                for (int j = 0; j < width[i]; j++)
                    b.append ("-");
                b.append ("+");
            }

            System.out.println (b.toString ( ));
            System.out.print ("|");
            for (int i = 1; i <= columnsCount; i++){
                System.out.print (md.getColumnName (i));
                for (int j = md.getColumnName (i).length ( ); j < width[i]; j++)
                    System.out.print (" ");
                System.out.print ("|");
            }
            System.out.println ( );
            System.out.println (b.toString ( ));

            
            while (rs.next())
            {
                ++rowsCount;
                System.out.print ("|");
                for (int i = 1; i <= columnsCount; i++){
                    String s = rs.getString(i);
                    if (rs.wasNull())
                        s = "Nera";
                    System.out.print (s);
                    for (int j = s.length(); j < width[i]; j++)
                        System.out.print(" ");
                    System.out.print("|");
                }
                System.out.println();
            }

            System.out.println (b.toString ( ));
            System.out.println ("Įrašų kiekis: " + rowsCount);
        } else {
            throw new SQLException("Empty result set");
        }
    }
}
