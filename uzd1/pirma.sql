--SELECT isbn, pavadinimas, leidykla, verte AS "kaina", verte*2 AS "kaina x2" 
--FROM Stud.knyga
--WHERE verte BETWEEN 5 AND 20
--WHERE leidykla = 'Baltoji'
--ORDER BY 2 DESC;
--psql -h pgsql2.mif -f pirma.sql biblio
SELECT pavadinimas
FROM Stud.knyga
EXCEPT
SELECT isbn
FROM Stud.knyga
