--psql -h pgsql2.mif -f 1-4.sql biblio
--Vardai ir pavardės skaitytojų, kurie yra paėmę mažiau knygų už visų skaitytojų paimtų knygų vidurkį.
CREATE TEMP TABLE laik AS(
SELECT 	s.vardas AS vardas,
		s.pavarde AS pavarde,
		COUNT(e.isbn) AS paimtos
FROM	Stud.Egzempliorius AS e
		JOIN Stud.Skaitytojas AS s
			ON e.skaitytojas = s.nr
GROUP BY s.nr);

CREATE TEMP TABLE vid AS(
SELECT 	AVG(paimtos) AS vidurkis,
		MAX(paimtos) AS maximumas
FROM	laik
);

SELECT 	vardas, pavarde, laik.paimtos--, vid.vidurkis
FROM 	laik, vid
WHERE 	laik.paimtos < vid.vidurkis OR
		laik.paimtos = vid.maximumas;
