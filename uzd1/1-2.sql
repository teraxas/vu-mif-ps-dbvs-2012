--psql -h pgsql2.mif -f 1-2.sql biblio
--Visų ši mėnesį turinčių gražinti knygas skaitytojų vardai ir pavardės.
--viktoras.ciumanovas@gmail.com
SELECT s.nr, s.vardas, s.pavarde, e.nr
FROM Stud.Skaitytojas AS s 
LEFT OUTER JOIN Stud.Egzempliorius AS e ON e.skaitytojas = s.nr
AND s.vardas = 'Jonas'
--HAVING 
--WHERE extract(month from current_date) = extract(month from e.grazinti) 
--AND s.nr = e.skaitytojas;

--visi skaitytojai, egzemplioriaus nr, jei skaito. gali kartotis
