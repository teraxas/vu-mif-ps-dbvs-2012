--psql -h pgsql2.mif -f 1-5.sql biblio
--Konkrečiam vartotojui, skaičius virtualių lentelių, kurioms jis turi teisę rašyti užklausas.
SELECT tab.*--COUNT(*)
FROM Information_Schema.table_privileges AS tp
	JOIN Information_Schema.tables AS tab
	ON tp.table_schema = tab.table_schema
		AND tp.table_name = tab.table_name
WHERE tp.grantee = 'kajo0241'
	AND tp.privilege_type = 'SELECT'
	AND tab.table_type = 'VIEW'
