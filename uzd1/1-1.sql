--psql -h pgsql2.mif -f 1-1.sql biblio
--Vardai, pavardės ir AK visų skaitytojų, kurių AK prasideda konkrečiu skaitmeniu.
SELECT vardas, pavarde, ak AS "Asmens kodas" 
FROM Stud.Skaitytojas 
WHERE RIGHT(ak, 1) = '2'
ORDER BY pavarde DESC

