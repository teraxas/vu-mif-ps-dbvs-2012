--psql -h pgsql2.mif -f 1-3.sql biblio
--Kiekvieniems metams, kai buvo išleista bent viena knyga, visų egzempliorių ir paimtų egzempliorių skaičiai.

SELECT  k.metai, 
		COUNT(e.isbn) AS viso, 
		COUNT(e.skaitytojas) AS paimta,
		COUNT(DISTINCT e.isbn) AS knygos,
		--RIGHT(CAST(k.metai AS CHAR(4)),1),--TEST
		--CAST(COUNT(e.skaitytojas) AS CHAR) ,--TEST
		CASE WHEN MOD(k.metai,10) = COUNT(e.skaitytojas)
					  THEN '1' ELSE 0
					  END AS mpslp
FROM 	stud.egzempliorius as e
		JOIN stud.knyga as k
		ON e.isbn = k.isbn
GROUP BY k.metai
HAVING MOD(k.metai,10) = COUNT(e.skaitytojas)
ORDER BY k.metai

--1 jei metų pask skaitmuo = paimtu, 0, jei ne
